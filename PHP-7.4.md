# **EXERCISE**

**Task:** Install PHP 7.4 on your local linux machine using the ppa:ondrej/php package repo.

**Instruction:**

* Learn how to use the add-apt-repository command.
* Submit the content of /etc/apt/sources.list and the output of php -v command.
  
**Here is what i did;**

1. First, i ensured that my linux machine is in its latest status by updating the system.

<sudo apt update && apt upgrade> 

2. Only the latest version of PHP can be installed using default Ubuntu20.04 system repository. Hence, to get the older version, i added software properties common and the PPA repository called Ondrej.

<sudo apt install software-properties-common -y>

<sudo add-apt-repository ppa:ondrej/php -y>

3. Finally, i installed PHP7.4 with the following command. 
Note: I mentioned the version number with the command otherwise the system would install the latest version of PHP.

<sudo apt install php7.4>


**Result:**

/ETC/APT/SOURCES.LIST OUTPUT

![/etc/apt/sources.list](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/etc%20apt%20sources%20list.png?raw=true)


PHP -V COMMAND OUTPUT

![php -v command output](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/php-v.png?raw=true)