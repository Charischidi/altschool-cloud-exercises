# **10 CIS Benchmarks**

**Exercise**

**Task:** Review the CIS benchmark for Ubuntu and try to implement at least 10 of the recommendations that has been made.
You can do more to practice more.


* **Configure Software Updates:**
    Systems need to have package manager repositories configured to ensure they receive the latest patches and updates.

    Verify if package repositories are configured correctly: <apt-cache policy

    Thanks to Ubuntu as it features a comprehensive package management system for installing, upgrading, configuring, and removing software. Ubuntu’s package manager, apt, has a well-established workflow for performing a full system upgrade.

    Ubuntu also provides a unique tool called unattended-upgrades in order to automatically retrieve and install security patches and other essential upgrades for servers. Most Ubuntu servers come with this tool automatically installed and configured, but it can be installed with the following apt commands: <sudo apt update 
    <sudo apt install unatte
    
    After installation, check to ensure that the unattended-upgrades service is running using systemctl: <sudo systemctl status unattended-upgrades.service

    If a system's package repositories are misconfigured, important patches may not be identified or a rogue repository could introduce compromised software.
    
* **Ensure GPG keys are configured:**
    Most packages    managers implement GPG key signing to verify package integrity during installation.
  
    It is important to ensure that updates are obtained from a valid source to protect against spoofing that could lead to the inadvertent installation of malware on the system.

    Verify GPG keys are configured correctly for your package manager: <apt-key list
    
    apt-key is a utility used to manage the keys that APT uses to authenticate packages.

    Update your package manager GPG keys in accordance with site policy.

* **Filesystem Integrity Checking**
**(Ensure AIDE is installed)**

    One of the most popular tools for monitoring changes to a Unix or Linux system is known as Advanced Intrusion Detection Environment (AIDE) originally written by Rami Lehti and Pablo Virolainen in 1999.

    AIDE is a file integrity checking tool. While it cannot prevent intrusions, it can detect unauthorized changes to configuration files by alerting when the files are changed.

    AIDE takes a snapshot of filesystem state including modification times, permissions, and file hashes which can then be used to compare against the current state of the filesystem to detect modifications to the system.

    By monitoring the filesystem state compromised files can be detected to prevent or limit the exposure of accidental or malicious misconfigurations or modified binaries.

    To verify if AIDE is installed, run the following command: 
    
    <dpkg-query -W -f='${binary:Package}\t${Status}\t${db:Status-Status}\n' aide aide-common>

    To install AIDE, using the following command: <apt install aide aide-common

    Run the following commands to initialize AIDE:
    
    <aideinit
    
    <mv /var/lib/aide/aide.db.new /var/lib/aide/aide.db

    The prelinking feature can interfere with AIDE because it alters binaries to speed up their start up times. Run <prelink -ua> to restore the binaries to their prelinked state, thus avoiding false positives from AIDE.

* **Configure Time Synchronization:**
    It is recommended that physical systems and virtual guests lacking direct access to the physical host's clock be configured to synchronize their time using a service such as systemd-timesyncd, chrony, or ntp. And ensure only one time synchronization method is used on the system.

    Accurate timekeeping is integral to modern software deployments. Without it, you may encounter data corruption, errors, and other issues that are difficult to debug. Time synchronization can help ensure your logs are being recorded in the correct order, and that database updates are appropriately applied.
    
    Fortunately, Ubuntu 20.04 has Coordinated Time Synchronization(UTC) built-in and activated by default using systemd’s timesyncd service.

    UTC is Coordinated Universal Time, the time at zero degrees longitude. While this may not reflect our current time zone, using Universal Time prevents confusion when infrastructures spans multiple time zones.

    To view the time on a system, we use the command <date
    
    For example, to change our time zone to Africa/Lagos; we set the time zone with <sudo timedatectl set-timezone  Africa/Lagos>

    We can verify our changes by running date command again:
    <date

   
* **(Firewall Configuration)**
   **Configure Uncomplicated Firewall:**
    A firewall can be defined as a system of network security that controls and filters the traffic on the rule's predefined set. So a firewall is a set of rules. It is an intermediary system between the Internet and the device. 

    Uncomplicated Firewall (ufw) - Provides firewall features by acting as a front-end for the Linux kernel's netfilter framework via the iptables backend. ufw supports both IPv4 and IPv6 networks.
    
    UFW, or Uncomplicated Firewall, is a simplified firewall management interface that hides the complexity of lower-level packet filtering technologies such as iptables and nftables.

    To check if ufw is enabled, run: <sudo ufw status

    To enable UFW on your system, run: <sudo ufw enable

    If for some reason you need to disable UFW, you can do so with the following command: <sudo ufw disable
    
    Changing firewall settings while connected over network can result in being locked out of the system, so be cautious.

* **Ensure journald is configured to compress large log files:**
    The journald system includes the capability of compressing overly large files to avoid filling up the system with logs or making the log's size unmanageable.
    
    Note: The main configuration file /etc/systemd/journald.conf is read before any of the custom *.conf files. If there are custom configs present, they override the main configuration parameters

    Uncompressed large files may unexpectedly fill a filesystem leading to resource unavailability. Compressing logs prior to write can prevent sudden, unexpected filesystem impacts.

    Solution
    Edit the /etc/systemd/journald.conf file and add the following line:

    Compress=yes

    Restart the service: <systemctl restart systemd-journald

* **Configure time-based job schedulers:**
    Cron is a time-based  scheduler. It allows server administrators to run scripts known as cron jobs automatically at scheduled intervals.

    Cron can run simple commands, shell scripts, and programs written in languages such as PHP, Python, and Perl. If a task can be completed on the command line or in a script, you can automate it with the scheduler.

    Almost every Linux distribution has some form of cron installed by default. However, for Ubuntu machine on which cron isn’t installed, it can be installed using APT.

    Before installing cron on an Ubuntu machine, it's important to update computer’s local package index: <sudo apt update

    Then install cron with the following command: <sudo apt install cron 
    
    And ensure sure it’s set to run in the background too: <sudo systemctl enable cron

    Cron jobs are recorded and managed in a special file known as a crontab. Each user profile on the system can have their own crontab where they can schedule jobs, which is stored under /var/spool/cron/crontabs/.

* **Ensure permissions on /etc/crontab are configured:**
    The /etc/crontab file is used by cron to control its own jobs. The commands in this item make sure that root is the user and group owner of the file and that only the owner can access the file.

    This file contains information on what system jobs are run by cron. Write access to these files could provide unprivileged users with the ability to elevate their privileges. Read access to these files could provide users with the ability to gain insight on system jobs that run on the system and could provide them a way to gain unauthorized privileged access.

    Solution

    Run the following commands to set ownership and permissions on /etc/crontab:

    chown root:root /etc/crontab

    chmod u-x,og-rwx /etc/crontab

    OR

    Run the following command to remove cron:

    yum remove cronie

* **Ensure sudo log file exists:**
    sudo is a program for Unix-like computer operating systems that enables users to run programs with the security privileges of another user, by default the superuser. 
    
    Sudo (su “do”) allows a system administrator to delegate authority, to give certain users (or groups of users) the ability to run some (or all) commands as root or another user while providing an audit trail of the commands and their arguments. sudo can use a custom log file. 
    
    A sudo log file simplifies auditing of sudo commands.

    Solution

    Edit the file /etc/sudoers or a file in /etc/sudoers.d/ with visudo -f and add the following line:

    Defaults logfile='<PATH TO CUSTOM LOG FILE>'

    Example:
    
    Defaults logfile='/var/log/sudo.log'

    Note: visudo edits the sudoers file in a safe fashion, analogous to vipw(8). visudo locks the sudoers file against multiple simultaneous edits, provides basic sanity checks, and checks or parse errors. If the sudoers file is currently being edited you will receive a message to try again later.

* **Ensure access to the su command is restricted:**
    The su command allows a user to run a command or shell as another user. The program has been superseded by sudo, which allows for more granular control over privileged access. Normally, the su command can be executed by any user.
    
    By uncommenting the pam_wheel.so statement in /etc/pam.d/su, the su command will only allow users in a specific groups to execute su. This group should be empty to reinforce the use of sudo for privileged access.

    Solution:
    Create an empty group that will be specified for use of the su command. The group should be named according to site policy.

    Example:
    groupadd sugroup

    Add the following line to the /etc/pam.d/su file, specifying the empty group:

    auth required pam_wheel.so use_uid group=sugroup

    Restricting the use of su , and using sudo in its place, provides system administrators better control of the escalation of user privileges to execute privileged commands. The sudo utility also provides a better logging and audit mechanism, as it can log each command executed via sudo , whereas su can only record that a user executed the su program.