# **Bash Script**

**Exercise**

**Task:**
Create a bash script to run at every hour, saving system memory (RAM) usage to a specified file and at midnight it sends the content of the file to a specified email address, then starts over for the new day.

**Instruction:**
Submit the content of your script, cronjob and a sample of the email sent, all in the folder for this exercise.

**STEPS TAKEN;**

1) First, I created an email account on Mailtrap.
2) Then, I installed SSMTP & Mailutils and configured Mailtrap settings accordingly in /etc/ssmtp/ssmtp.conf
3) I verified if cron is already installed and running using the following  command; sudo systemctl status cron 
4) Then, I created a directory testlogs, mkdir testlogs
5) Go into the directory and created a bash file, cd testlogs && touch testram_memusage.sh
6) I also created log file for memory logs storage, touch testram_memusage.log
7) I made the bash file executable for the user, chmod u+x testram_memusage.sh
8) I also added the same permission to the memory log file, chmod u+x testram_memusage.log
9) Opened testram_memusage.sh with a nano text editor and wrote a script such that at midnight it will send a mail with the attached memory log file to my email.
10) I opened crontab -e and added cronjob such that it will trigger the script to run at hourly intervals.


Script


![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Script.jpeg?raw=true)


Cronjob


![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Crontab.jpeg?raw=true)


Mail Sample


![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Received%20mail.jpeg?raw=true)


Memory log


![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Memory%20log.jpeg?raw=true)