# **10 Linux Commands**

**Exercise 2**

**Task:**
Research online for 10 more linux commands aside the ones already mentioned in this module. 

Submit using your altschool-cloud-exercises project, explaining what each command is used for with examples of how to use each and example screenshots of using each of them.

**Instruction:**
Submit your work in a folder for this exercise in your altschool-cloud-exercises project. 
You will need to learn how to embed images in markdown files.

**Instruction:** Submit your work in a folder for this exercise in your altschool-cloud-exercises project. You will need to learn how to embed images in markdown files.
  
  
**Result:**
1. **echo $PATH**  *This* outputs a list of directories where executable files are stored.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/echo%20path%20command.png?raw=true)

2. **du** *It* allows to check how much space a file directory takes.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/du%20command.png?raw=true)

3. **ping** *This* allows us to check connectivity status to a server using the server ip address.
   
![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ping%20command.png?raw=true)


4. **echo $VARIABLE** *This* is used to display the value assigned to a variable. For example i assigned a name chika to a variable My_Name, then echo the variable My_Name.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/echo%20variable%20command.png?raw=true)

5. **unset** *This* allows us to delete a variable. Eg, I used unset to delete the the variable My_Name.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/unset%20command.png?raw=true)

6.  **sudo adduser username** *This* allows us to add a new user to linux system. Example, i added a user edozie to my linux machine, then i ran command (tail /etc/group) to show that the user is added.  

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/sudo%20adduser%20username.png?raw=true)

7.  **grep** *This* is used to search for a specific string within an output. After any command on the terminal, pipe (|) the output to the grep command and extract the required string. For example, to check if a name chidi exist, i pipe the output of (tail /etc/group) command to grep and retrieved the name.
   
![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/grep%20command.png?raw=true)

8. **ssh** *It* allows us to connect to an external machine on the network with the use of the machine ip address.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ssh%20command.png?raw=true)

9. **mkdir and rmdir** *These* linux commands comes handy when you want to create and remove directory. **mkdir** allows us to create a directory while **rmdir** allows us to remove directory.

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/mkdir%20and%20rmdir%20command.png?raw=true)

10. **chmod and chown** *These* are a must-know commands in linux administration. The difference is that **chmod** allows us to change file permissions while **chown** allows us to change file ownership.
    
![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/chomd%20command.png?raw=true)

![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/chown%20command.png?raw=true)
