# **Exercise**

**Task:**

193.16.20.35/29

What is the;

* Network IP

* Number of hosts

* Range of IP addresses

* Broadcast IP 

**Instruction:** 
Submit all your answer as a markdown file in the folder for this exercise.

**Solution**

* **NETWORK IP**

First, convert IP Address to its binary values using the golden chart.
128 64 32 16 8 4 2 1

11000001 00010000 00010100 00100011

The Classless In the Domain Routing(CIDR) /29 tells us the number of the IP Address binary bits that are turned on (Netmask of the Subnet value).

11111111 11111111 11111111 11111000

Network IP is the Logical AND Operation of the respective bits in the binary representation of the IP Address and the Subnet value.

Therefore, the result becomes;

11000001 00010000 00010100 00100000

Converting back to decimal values to get the Network Ip;

193 16 20 32

**So, the Network IP is 193. 16. 20. 32**

* **NUMBER OF HOSTS**

Having known the Netmask (Number of bits turned on /29), we can apply the folllowing formular to get the number of hosts.

Max no of host = 2^(32-Netmask_lenght) - 2

Therefore; 2^(32-29) - 2

= 2^3 - 2

= 8 - 2

= 6

**So, the number of hosts is 6**

**RANGE OF IP ADDRESSES**

The range of IP Addresses lie between the value of the Network IP and the Broadcast IP.

So we will determine the Broadcast IP first.

* **BROADCAST IP** 

Broadcast IP is achieved by the Logical OR Operation of the IP Address and a new subnet value.

Therefore to get the new Subnet value, convert the host bits of the original subnet value to ones and the Network bits to zeros.

The result becomes;

Original Subnet Value: 11111111 11111111 11111111 11111000

New Subnet Value: 00000000 00000000 00000000 00000111

So, Logical OR Operation of the New Subnet value and the IP Address binary value will give us the following;

New Subnet value: 00000000 00000000 00000000 00000111

IP Address binary value: 11000001 00010000 00010100 00100011

= 11000001 00010000 00010100 00100111

It is then converted to deimal value;

193 16 20 39

**Therefore, the Broadcast IP is 193. 16. 20. 39**

* **RANGE OF IP ADDRESSES**

Having determined the Network IP and Broadcast IP, we can now calculate the range of IP Addresses knowing that they lie between the Network IP (193. 16. 20. 32) and (193. 16. 20. 39)

Therefore, they are;

193.16.20.33

193.16.20.34

193.16.20.35

193.16.20.36

193.16.20.37

193.16.20.38

