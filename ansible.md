

**Exercise**

**Task:**
* Create an Ansible Playbook to setup a server with Apache

* The server should be set to the Africa/Lagos Timezone

* Host an index.php file with the following content, as the main file on the server:

  < ?php
date("F d, Y h:i:s A e", time());
? >

**Instruction:**
Submit the Ansible playbook, the output of systemctl status apache2 after deploying the playbook and a screenshot of the rendered page.

**Here is what i did;**

1. Installation of ansible and its dependencies using this commands: 

   <sudo apt install software-properties-common -y python-apt >

   <sudo apt install ansible -y >

2. Generated SSH Key pair for my ansible control node(local Ubutu20.04-lts Machine) and copied the public key to my managed node (remote Ubuntu20.04-lts machine) using this command:
   <ssh-copy-id -i ~/.ssh/id_rsa.pub user@ip_address>


3. Created a directory ansible_apache and cd ansible_apache.

4. Created a host inventory named inventory_apache and defined the host inside.

5. Created ansible config file named ansible.cfg and set some defaults inside.
Basically, i defined inventory_apache as a default inside the ansible config file, so that ansible can read it anytime i run ansible command. 
 
6. Pinged the managed node to test availability/connectivity using this command: 
   
   <ansible all -i host-inventory -m ping> OR
   <ansible all -m ping >

7. Created playbook named playbook_apache.yml and defined all the task to be executed by ansible.

8. Created a PHP file named apache_index.php and added its content.

9. Run a check with ansible command:
   <ansible-playbook -i host-inventory playbook.yml --check>

10. Satisfied with the result of the check, i removed the --check and run the ansible-playbook to get the work done. 
   <ansible-playbook -i host-inventory playbook.yml>

   **Result**


   Ansible Playbook

   
   ![ansible plabook](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Ansible%20playbook.png?raw=true)

   
   Systemctl Status Apache2 

   
   ![systemctl status apache2](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/systemctl%20status%20apache2.png?raw=true)

   
   The Rendered Page After Deployment

   
   ![deployment rendered page](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/Ansible%20Deployed.png?raw=true)


   Apache Server Tested


   ![](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/IP%20address%20tested.png?raw=true)