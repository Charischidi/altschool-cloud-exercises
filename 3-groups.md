# **EXERCISE**

**Task:**
* Create 3 groups – admin, support & engineering and add the admin group to sudoers. 
* Create a user in each of the groups. 
* Generate SSH keys for the user in the admin group

**Instruction:** Submit the contents of /etc/passwd, /etc/group, /etc/sudoers


**RESULTS**

/ETC/PASSWD
![/etc/passwd](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ETC%20PASSWD%20OUTPUT.png?raw=true)

/ETC/GROUP
![/etc/group](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ETC%20GROUP%20OUTPUT.png?raw=true)

/ETC/SUDOERS
![/etc/sudoers](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ETC%20SUDOERS%20OUTPUT.png?raw=true)

SSH KEY FOR USER IN ADMINGRUOP
![AdminGroup User SSH Key](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/SSH%20KEYS%20FOR%20USER%20IN%20AdminGroup.png?raw=true)