# **Setting Up Ubuntu20.04-lts**

**EXERCISE 1**

**TASK:** Setup Ubuntu 20.04 LTS on your local machine using Vagrant.

**INSTRUCTION:** Customize your Vagrantfile as necessary with private_network set to dhcp
Once the machine is up, run ifconfig and share the output in your submission along with your Vagrantfile in a folder for this exercise.

**HERE IS WHAT I DID:**
* First, i ensure virtualization is turned on in my Laptop BIOS.
* I downloaded and installed Oracle VM VirtualBox.
* I downloaded and installed Vagrant, AMD64 version which is for 64bit computer.
* Using Git Bash on Window Laptop, i created a folder named Virtual-Machine, then cd into the folder and created another folder named Ubuntu20.04-lts. 
* Already inside Virtual-Machine folder. i cd into Ubuntu20.04-lts and initialize Vagrant using vagrant init ubuntu/focal64. This created a Vagrantfile.
I then edited the Vagrantfile using Vs Code by navigating to the commented line # config.vm.network "private_network", ip: "192.168.33.10 then uncommented the line by removing the # symbol at the beginning of the line and then changed the ip: "192.0.38.12" to type: "dhcp" and saved. 
* Back to Git bash terminal and inside Ubuntu20.04-lts folder, i ran the command vagrant up. This command setup and bring up the virtual machine.
* As the machine is powered up and running, i ran the command vagrant ssh. This command logged me into the virtual machine.
* Being inside the virtual machine, i installed net-tools using the command sudo apt install net-tools. This command installed the necessary network utilities.
* Finally, i ran the command ifconfig. This command dispalyed my networks and assigned ip.


**Results:**

VAGRANTFILE CONTENT

# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/focal64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
    config.vm.network "private_network", type: "dhcp"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end


IFCONFIG

![ifconfig Output](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/ifconfig%20Output.png?raw=true)