# **EXERCISE**

**Task:**
* You already have Github account, also setup a GitLab account if you don’t have one already.
* You already have a altschool-cloud-exercises project, clone the project to your local system
* Setup your name and email in Git’s global config


**Instruction:** Submit the output of git config -l, git remote -v, and git log


**Here is what i did;**

1. I cloned the altschool-cloud-exercises repository from my Github account to my local system using this command <git clone repo_url
 
2. I cd into the cloned repository.

3. <git status> showed there was no changes made. Otherwise i would have added the changes with <git add .>  and commit with <git commit -m "commit statement">

4. It created a repository (aka Project) in GitLab account and copied the url from the repos clone area.

5. Then i set the url to point to Gitlab < git remote add origin gitlab_repo_url>

6. I checked with <git remote -v> to ensure the url is pointing to GitLab.

7. Finally, <git push -u -f> or <git push -u -f origin gitlab_repo_url>  


**Result:**

GIT CONFIG -L

![git config -l](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/git%20config%20-l.png?raw=true)

GIT REMOTE -V

![git remote -v](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/git%20remote%20-v.png?raw=true)

GIT LOG

![git log](https://github.com/Charischidi/Alt_School-Cloud-Exercises/blob/main/Screenshorts/git%20log.png?raw=true)